/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author ASUS
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("Da");
        Plane plane = new Plane("Engine number I");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("Naroto");
        Car car = new Car("Engine number one");

        Flyable[] flyables = {bat, plane};
        for (Flyable f : flyables) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startEngine();
                p.stopEngine();
                p.ralseSpeed();
                p.applyBreak();
                p.run();
            }
            f.fly();
        }
        Runable[] runables = {dog, plane, car};
        for (Runable r : runables) {
            r.run();

        }

    }
}
